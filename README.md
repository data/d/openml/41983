# OpenML dataset: CIFAR-100

https://www.openml.org/d/41983

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset is just like the CIFAR-10, except it has 100 classes containing 600 images each. There are 500 training images and 100 testing images per class. The 100 classes in the CIFAR-100 are grouped into 20 superclasses. Each image comes with a "fine" label (the class to which it belongs) and a "coarse" label (the superclass to which it belongs).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41983) of an [OpenML dataset](https://www.openml.org/d/41983). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41983/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41983/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41983/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

